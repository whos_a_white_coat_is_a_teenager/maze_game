import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;
import java.util.TimerTask;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MazeGame {

	public static void main(String[] args) {
		JFrame frame = new JFrame("迷宫游戏");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(new MazeGamePanel());
		frame.pack();
		frame.setVisible(true);
	}

}
class MazeGamePanel extends JPanel {// 主面板:显示迷宫以及定义相关操作.
	private final int DELAY = 10,CHASEDELAY=3500,GAMETIME=3*60*1000;//定时器延迟时间,无墙模式追赶者移动延迟时间
	private final int checkSizeDefault = 20, widthDefault = 48,
			heightDefault = 23, numberOfChasersDefault = 20,
			tearDownAllowedDefault = 10,timeSurplusDefault=180;// 默认值
	private final int panelWidth = 1024, panelHeight = 650;// 主面板宽,高
	private int checkSize, width, height, numberOfChasers, tearDownAllowed,timeSurplus;
	// 迷宫的单位方格大小(以像素为单位),迷宫的宽度(方格数),迷宫的高度(方格数),追赶者数目,玩家可拆墙次数限制,剩余时间
	private Maze maze;// 一个迷宫面板
	private JButton tipButton,solve, newMaze, chase, newSure,newCancel,noWalls,miExit;
	private boolean tipControl,solveControl,chaseControl;//开关按键控制:true为目前开启,false为目前关闭
	private JLabel help, tip, tearDownAllowedTimes,tearGameTimer;// 帮助标签,提示标签
	private Timer timer,timer2;// 定时器:每隔10毫秒就获取当前游戏结果和当前提示.注意:迷宫开始,计时器开始;迷宫停止(指玩家输或赢),计时器停止
	// (由于玩家不动也有可能被追赶成功,因此不推荐在每次移动时作出判断)
	private Timer time3;
	private JLabel checkSizeLabel, widthLabel, heightLabel,
			numberOfChasersLabel, tearDownAllowedLabel,timeSurplusLabel;// 滑动条提示
	private JSlider checkSizeSlider, widthSlider, heightSlider,
			numberOfChasersSlider, tearDownAllowedSlider,timeSurplusSlider;// 滑动条
	private JPanel mazeMaker;// 迷宫制造者:通过滑动条制造迷宫.仅当玩家点击"制造新迷宫"或输或赢时才可见
	private JScrollPane mazeContainer;//存放迷宫面板的滚动窗格
	private boolean noWallsControl;//无墙模式控制
	
	public MazeGamePanel() {
		checkSize = checkSizeDefault;// 初始化一个迷宫:方格边长:20像素
		width = widthDefault;// 宽35个方格
		height = heightDefault;// 高25个方格.即迷宫面板大小为500*700
		numberOfChasers = numberOfChasersDefault;// 默认十个个追赶者
		tearDownAllowed = tearDownAllowedDefault;// 默认能拆十次墙
		timeSurplus = timeSurplusDefault;//默认倒计时180秒

		maze = new Maze(checkSize, width, height, numberOfChasers,
				tearDownAllowed,timeSurplus);// 根据上面的设置来初始化迷宫

		tipButton=new JButton("开启/关闭 提示");
		tipControl=false;
		chase = new JButton("开启/关闭 追赶者");
		chaseControl=true;
		solve = new JButton("开启/关闭 答案");
		solveControl=false;
		
		newMaze = new JButton("制造新迷宫");
		
		noWalls=new JButton("无墙模式");
		noWallsControl=false;//默认有墙模式
		
		miExit=new JButton("退出");
		
		miExit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});
		
		newSure = new JButton("确定");
		newCancel = new JButton("取消");
		help = new JLabel(maze.help()+ "请 按 方 向 建 移 动. 按 W,S,A,D 拆除 上,下,左,右 墙.");
		//按键功能与Maze类无关
		tip = new JLabel(maze.currentTip());
		tearDownAllowedTimes = new JLabel("剩余可拆墙次数:"+ maze.tearDownAllowedTimes());
		
//======================================================================================
		
//=======================================================================================
		timer = new Timer(DELAY, new TimerListener());
		timer2 =new Timer(GAMETIME, new TimerListener1());
		timer.start();// 注意:迷宫开始,计时器开始;迷宫停止(指玩家输或赢),计时器停止
		timer2.start();//时间到停止

		maze.addKeyListener(new KeyBoardListener());// 迷宫面板添加键盘监听器
		maze.setFocusable(true);
		
		//迷宫制造者滑动条大小
		checkSizeSlider = new JSlider(JSlider.HORIZONTAL, 5, 50,
				checkSizeDefault);
		checkSizeSlider.setMajorTickSpacing(5);
		checkSizeSlider.setPaintTicks(true);
		checkSizeSlider.setPaintLabels(true);
		checkSizeSlider.setAlignmentX(Component.LEFT_ALIGNMENT);

		widthSlider = new JSlider(JSlider.HORIZONTAL, 10, 50, widthDefault);
		widthSlider.setMajorTickSpacing(5);
		widthSlider.setPaintTicks(true);
		widthSlider.setPaintLabels(true);
		widthSlider.setAlignmentX(Component.LEFT_ALIGNMENT);

		heightSlider = new JSlider(JSlider.HORIZONTAL, 10, 40, heightDefault);
		heightSlider.setMajorTickSpacing(5);
		heightSlider.setPaintTicks(true);
		heightSlider.setPaintLabels(true);
		heightSlider.setAlignmentX(Component.LEFT_ALIGNMENT);

		numberOfChasersSlider = new JSlider(JSlider.HORIZONTAL, 0, 50,
				numberOfChasersDefault);
		numberOfChasersSlider.setMajorTickSpacing(5);
		numberOfChasersSlider.setPaintTicks(true);
		numberOfChasersSlider.setPaintLabels(true);
		numberOfChasersSlider.setAlignmentX(Component.LEFT_ALIGNMENT);

		tearDownAllowedSlider = new JSlider(JSlider.HORIZONTAL, -1, 30,
				tearDownAllowedDefault);
		tearDownAllowedSlider.setMajorTickSpacing(5);
		tearDownAllowedSlider.setPaintTicks(true);
		tearDownAllowedSlider.setPaintLabels(true);
		tearDownAllowedSlider.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		timeSurplusSlider =new JSlider(JSlider.HORIZONTAL,60,200,timeSurplusDefault);
		timeSurplusSlider.setMajorTickSpacing(5);
		timeSurplusSlider.setPaintTicks(true);
		timeSurplusSlider.setPaintLabels(true);
		timeSurplusSlider.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		
		maze.setPreferredSize(new Dimension(panelWidth,panelHeight));
		//用setPreferredSize设置加入到滚动窗格的面板的大小才可以使滚动窗格有效
		mazeContainer=new JScrollPane(maze);//迷宫面板放到滚动窗格中
		mazeContainer.setEnabled(false);//这样可以使JScrollPane不响应键盘,只响应鼠标.这样走迷宫会好一点

		SliderListener listener = new SliderListener();
		checkSizeSlider.addChangeListener(listener);
		widthSlider.addChangeListener(listener);
		heightSlider.addChangeListener(listener);
		numberOfChasersSlider.addChangeListener(listener);
		tearDownAllowedSlider.addChangeListener(listener);
		timeSurplusSlider.addChangeListener(listener);

		checkSizeLabel = new JLabel("小方格边长(以像素为单位):" + checkSizeDefault);
		widthLabel = new JLabel("宽度(方格数):" + widthDefault);
		heightLabel = new JLabel("高度(方格数):" + heightDefault);
		numberOfChasersLabel = new JLabel("追赶者数目(可设为0):"
				+ numberOfChasersDefault);
		tearDownAllowedLabel = new JLabel("可拆墙限制次数(设为-1则不限次数,设为0则不能拆墙):"
				+ tearDownAllowedDefault);
		timeSurplusLabel =new JLabel("游戏时间（以秒为单位）:"+timeSurplusDefault);
		

		ButtonListener bListener = new ButtonListener();
		chase.addActionListener(bListener);
		solve.addActionListener(bListener);
		tipButton.addActionListener(bListener);
		newMaze.addActionListener(bListener);
		noWalls.addActionListener(bListener);
		newSure.addActionListener(bListener);
		newCancel.addActionListener(bListener);// 添加按键监听器
		miExit.addActionListener(bListener);

		JLabel title=new JLabel("迷     宫     制     造     者");
		mazeMaker = new JPanel();
		mazeMaker.setLayout(new BoxLayout(mazeMaker, BoxLayout.Y_AXIS));
		mazeMaker.add(title);
		mazeMaker.add(Box.createRigidArea(new Dimension(0, 40)));//上下间距，多少像素点
		mazeMaker.add(checkSizeLabel);//添加提示
		mazeMaker.add(checkSizeSlider);//添加滑动条
		mazeMaker.add(Box.createRigidArea(new Dimension(0, 20)));
		mazeMaker.add(widthLabel);
		mazeMaker.add(widthSlider);
		mazeMaker.add(Box.createRigidArea(new Dimension(0, 20)));
		mazeMaker.add(heightLabel);
		mazeMaker.add(heightSlider);
		mazeMaker.add(Box.createRigidArea(new Dimension(0, 20)));
		mazeMaker.add(numberOfChasersLabel);
		mazeMaker.add(numberOfChasersSlider);
		mazeMaker.add(Box.createRigidArea(new Dimension(0, 20)));
		mazeMaker.add(tearDownAllowedLabel);
		mazeMaker.add(tearDownAllowedSlider);
		mazeMaker.add(Box.createRigidArea(new Dimension(0, 20)));
		mazeMaker.add(timeSurplusLabel);
		mazeMaker.add(timeSurplusSlider);
		
		
		
		JPanel controls = new JPanel();
		controls.setLayout(new BoxLayout(controls, BoxLayout.X_AXIS));
		controls.add(newSure);
		controls.add(newCancel);
		mazeMaker.add(Box.createRigidArea(new Dimension(0, 50)));
		mazeMaker.add(controls);
		mazeMaker.add(Box.createRigidArea(new Dimension(0, panelHeight)));
		// 加上以上语句,在显示mazeMaker的时候就不会显示到迷宫,比较美观
		mazeMaker.setPreferredSize(new Dimension(panelWidth, panelHeight));
		mazeMaker.setVisible(false);// 仅当玩家点击"制造新迷宫"或输或赢时才可见

		JPanel labels = new JPanel();
		labels.setLayout(new BoxLayout(labels, BoxLayout.Y_AXIS));
		labels.add(tip);
		labels.add(tearDownAllowedTimes);		
		//===============================================
		//倒计时
		showTime(labels);	
		//==================================================
		labels.add(help);
		JPanel controlPanel = new JPanel();// 控制面板
		controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.X_AXIS));
		controlPanel.add(tipButton);
		controlPanel.add(chase);
		controlPanel.add(solve);
		controlPanel.add(newMaze);//1
		controlPanel.add(noWalls);
		controlPanel.add(miExit);//2
		
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(mazeMaker);//迷宫制造者面板
		add(Box.createRigidArea(new Dimension(0, 20)));
		add(mazeContainer);//存放迷宫的滚动窗格
		add(Box.createRigidArea(new Dimension(0, 20)));
		add(labels);//所有标签存放的面板
		add(Box.createRigidArea(new Dimension(0, 20)));
		add(controlPanel);//控制面板:追赶者开关,答案开关,新迷宫按键,无墙，退出
		setPreferredSize(new Dimension(panelWidth, panelHeight));
	}

	private void showTime(JPanel labels) {
		java.util.Timer timer3 = new java.util.Timer();
		timer3.schedule(new TimerTask() {
			int i=timeSurplus;
			@Override
			public void run() {
				if (i==0) {
					timer3.cancel();
				}
				if (tearGameTimer != null) {
					labels.remove(tearGameTimer);
				}
				int s = i--;
				//System.out.println(s+".........");
				tearGameTimer =new JLabel("游戏剩余时间："+s);
				labels.add(tearGameTimer);
				labels.updateUI();
			}
		}, 0,1000);
	}
	
	private class TimerListener1 implements ActionListener{
		
		public void actionPerformed(ActionEvent e) {
			//================================
			tearGameTimer.setText("剩余游戏时间："
					+maze.timeSurplusTimes());
			//游戏时间到了，停止游戏。
			timer.stop();
			timer2.stop();
			
			// 必须要在此停止,否则因为timer仍在计时
			int another = JOptionPane.showConfirmDialog(null,
					"游戏时间到，你输了! 现在要生成一个新迷宫并重新开始?");
			if (another == JOptionPane.YES_OPTION) {
				timer.stop();
				mazeMaker.setVisible(true);// 玩家确认制造新迷宫,则把迷宫制造者设为可见
			}
			maze.requestFocusInWindow();
		}
	}

	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(tipControl==true)
			tip.setText(maze.currentTip());// 提示和可拆墙剩余次数最好都随时更新
			else
				tip.setText("提示已关闭");
				
			tearDownAllowedTimes.setText("剩余可拆墙次数:"
					+ maze.tearDownAllowedTimes());


			// 以下为判断当前玩家输赢状态
			if (maze.currentResult() == 1) {// 玩家已经到达终点
				timer.stop();
				// 必须要在此停止,否则因为timer仍在计时且两个if其中之一直成立
				int another = JOptionPane.showConfirmDialog(null,
						"恭喜到达终点! 现在要生成一个新迷宫并重新开始?");
				if (another == JOptionPane.YES_OPTION) {
					timer.stop();
					mazeMaker.setVisible(true);// 玩家确认制造新迷宫,则把迷宫制造者设为可见
				}
				maze.requestFocusInWindow();// 迷宫面板必须要重获焦点(不能把这句放在if语句外,因为timer定时执行,会使maze不断获得焦点)
			} else if (maze.currentResult() == -1) {// 玩家输了
				
				timer.stop();
				// 必须要在此停止,否则因为timer仍在计时且两个if其中之一直成立
				int another = JOptionPane.showConfirmDialog(null,
						"你输啦! 现在要生成一个新迷宫并重新开始?");
				if (another == JOptionPane.YES_OPTION) {
					timer.stop();
					mazeMaker.setVisible(true);// 玩家确认制造新迷宫,则把迷宫制造者设为可见
				}
				maze.requestFocusInWindow();// 迷宫面板必须要重获焦点(不能把这句放在if语句外,因为timer定时执行,会使maze不断获得焦点)
			}
		}
	}

	private class KeyBoardListener extends KeyAdapter {
		public void keyPressed(KeyEvent e) {
			switch (e.getKeyCode()) {
			case KeyEvent.VK_UP:
				maze.move(2);
				break;
			case KeyEvent.VK_DOWN:
				maze.move(4);
				break;
			case KeyEvent.VK_LEFT:
				maze.move(1);
				break;
			case KeyEvent.VK_RIGHT:
				maze.move(3);
				break;
			case KeyEvent.VK_W: {
				maze.tearDown(2);
				tearDownAllowedTimes.setText("剩余可拆墙次数:"
						+ maze.tearDownAllowedTimes());
			}
				break;
			case KeyEvent.VK_S: {
				maze.tearDown(4);
				tearDownAllowedTimes.setText("剩余可拆墙次数:"
						+ maze.tearDownAllowedTimes());
			}
				break;
			case KeyEvent.VK_A: {
				maze.tearDown(1);
				tearDownAllowedTimes.setText("剩余可拆墙次数:"
						+ maze.tearDownAllowedTimes());
			}
				break;
			case KeyEvent.VK_D: {
				maze.tearDown(3);
				tearDownAllowedTimes.setText("剩余可拆墙次数:"
						+ maze.tearDownAllowedTimes());
			}
				break;
			case KeyEvent.VK_I://保护
				maze.protectEnabled(true);
				break;
			case KeyEvent.VK_O://非保护
				maze.protectEnabled(false);
				break;
			case KeyEvent.VK_P://可变范围拆墙
				maze.variableTearDown(3, 3);
				break;
			}
		}
	}

	private class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == chase){
				if(chaseControl==false){
				maze.chaseOn();chaseControl=true;}
				else 
					{maze.chaseOff();chaseControl=false;}
				}
			else if (e.getSource() == solve){
				if(solveControl==false){
				maze.solveOn();solveControl=true;}
				else {
					maze.solveOff();solveControl=false;}
			}
			else if (e.getSource() == tipButton){
				if(tipControl==true)tipControl=false;
				else
					tipControl=true;//在timer监听器中判断即可
			}
			else if (e.getSource() == newSure) {
				if(noWallsControl==true){
				mazeMaker.setVisible(false);// 玩家能点击到这个按键说明已被设为可见,现在要重设为不可见
				maze.noWalls(checkSizeSlider.getValue(),
						widthSlider.getValue(), heightSlider.getValue(),
						numberOfChasersSlider.getValue(),CHASEDELAY);
				timer.start();// 计时器与迷宫一起启动
				timer2.start();
				
				
				}else{
					mazeMaker.setVisible(false);// 玩家能点击到这个按键说明已被设为可见,现在要重设为不可见
					maze.newMaze(checkSizeSlider.getValue(),
							widthSlider.getValue(), heightSlider.getValue(),
							numberOfChasersSlider.getValue(), tearDownAllowedSlider
									.getValue(),timeSurplusDefault);
					timer.start();// 计时器与迷宫一起启动
					timer2.start();
				}
				if(tipControl==true)tipControl=false;
				if(chaseControl==false)chaseControl=true;
				if(solveControl==true)solveControl=false;//重置
			} else if (e.getSource() == newCancel)
				mazeMaker.setVisible(false);// 玩家能点击到这个按键说明已被设为可见,现在要重设为不可见
			else if(e.getSource()==noWalls){
				timer.stop();
				timer2.stop();
				noWallsControl=true;//设为无墙模式
				mazeMaker.setVisible(true);// 玩家点击制造无墙迷宫,则把迷宫制造者设为可见
			}
			else {//玩家按newMaze
				timer.stop();
				timer2.stop();
				noWallsControl=false;//设为有墙模式
				mazeMaker.setVisible(true);// 玩家点击制造新迷宫,则把迷宫制造者设为可见
			}
			maze.requestFocusInWindow();// 在按了键后迷宫面板必须要重获焦点
		}
	}

	private class SliderListener implements ChangeListener {
		public void stateChanged(ChangeEvent e) {// 注意:此方法若可执行,说明mazeMaker此时必定可见
			checkSizeLabel.setText("小方格边长(以像素为单位):"
					+ checkSizeSlider.getValue());
			widthLabel.setText("宽度(方格数):" + widthSlider.getValue());
			heightLabel.setText("高度(方格数):" + heightSlider.getValue());
			numberOfChasersLabel.setText("追赶者数目(可设为0):"
					+ numberOfChasersSlider.getValue());
			tearDownAllowedLabel.setText("可拆墙限制次数(设为-1则不限次数,设为0则不能拆墙):"
					+ tearDownAllowedSlider.getValue());
			timeSurplusLabel.setText("游戏时间，以秒为单位："+timeSurplusSlider.getValue());
		}

	}
}

class Maze extends JPanel{
	private final int DELAY=1200;// 追赶者走动延迟时间(注意:追赶者很聪明,因此走动延迟时间长一点)
	private int checkSize, width, height;// (迷宫由单位方格构成)单位方格的边长,迷宫的宽度,迷宫的高度
	private Wall[][] horizontal;// 单位水平墙.例如:若迷宫长度为3*5,则horizontal为4*5数组
	private Wall[][] vertical;// 单位竖直墙.例如:若迷宫长度为3*5,则horizontal为3*6数组
	private Check[][] checks;// 单位方格的状态(在遍历迷宫时需要).例如:若迷宫长度为3*5,则checks为3*5数组
	private int playerRow, playerColumn;// 玩家目前所在行,目前所在列
	private boolean[][] notMarked;// 单位方格是否有标记过(在随机生成迷宫时需要)
	private boolean solve;// 控制是否打印结果
	private Timer timer;// 定时器:追赶者每隔一段时间就会在迷宫中走动,玩家碰到追赶者就输.
	private boolean chase;// 追赶者开关器
	private Chaser[] chasers;// 追赶者们
	private int tearDownAllowed;// 玩家可拆墙次数限制
	private int timeSurplus;//剩余时间
	private int currentResult;// 当前玩家的输赢情况:-1表示玩家输,0表示仍在进行,1表示玩家赢

	private enum Check {// 遍历迷宫时,单位方格的状态:notFoot--未踏足过;notMain--踏足过但不在主路径上;
		// main--踏足过且在主路径上(主路径即从起点到终点的路径)
		notFoot, notMain, main;
	}

	private enum Wall {// 单位墙的存在状态: notExist--不存在;exist--存在
		notExist, exist;
	}

	/**
	 * 一个新的随机生成的迷宫(面板).墙壁为黑色,玩家为蓝色,追赶者为红色. 建议本迷宫面板背景设置为白色. 玩家初始在左上角起点.右下角为终点.
	 * 
	 * @param checkSize
	 *            (迷宫由单位方格构成)单位方格的边长,以像素输入
	 * @param width
	 *            迷宫的宽度.例如:宽度若为3即宽度为3个单位方格
	 * @param height
	 *            迷宫的高度.例如:高度若为3即高度为3个单位方格
	 * @param numberOfChasers
	 *            追赶者的数量(不要追赶者则为0)
	 * @param tearDownAllowed
	 *            玩家可拆墙次数限制(不可拆墙则为0,可拆无数次则为-1)
	 * @param timeSurplus
	 * 				游戏剩余时间，倒计时
	 */
	public Maze(int checkSize, int width, int height, int numberOfChasers,
			int tearDownAllowed,int timeSurplus) {
		this.checkSize = checkSize;
		this.width = width;
		this.height = height;
		this.tearDownAllowed = tearDownAllowed;
		this.timeSurplus = timeSurplus;
		this.playerRow = 0;
		this.playerColumn = 0;// 玩家初始在左上角的方格:起点
		this.solve = false;// 不打印结果
		this.timer = new Timer(DELAY, new TimerListener());// 追赶者移动定时器

		if (numberOfChasers < 1) {
			this.chase = false;// 玩家设定追赶者为"关"
			chasers = new Chaser[0];
		} else {
			this.chase = true;
			chasers = new Chaser[numberOfChasers];// 初始化追赶者
			for (int i = 0; i < numberOfChasers; i++) {
				Random gen = new Random();
				int ranRow, ranColumn;
				ranRow = gen.nextInt(this.height);
				ranColumn = gen.nextInt(this.width);
				chasers[i] = new Chaser(ranRow, ranColumn);
			}
			chaseOn();// 开启追赶者,使其移动
		}

		this.currentResult = 0;// 游戏进行中
		this.checks = new Check[this.height][this.width];// 迷宫为height*width个方格
		this.horizontal = new Wall[this.height + 1][this.width];
		// 单位水平墙.horizontalWalls有height*width-1个
		this.vertical = new Wall[this.height][this.width + 1];
		// 单位竖直墙.horizontalWalls有(height-1)*width个
		this.notMarked = new boolean[this.height][this.width];
		// 单位方格是否有标记过(在随机生成迷宫时需要)

		// 以下为迷宫具体初始化:
		// 全部方格状态初始化为"未踏足过"
		for (int i = 0; i < this.height; i++)
			for (int j = 0; j < this.width; j++)
				checks[i][j] = Check.notFoot;

		// 全部方格初始化为"未标记"
		for (int i = 0; i < this.height; i++)
			for (int j = 0; j < this.width; j++)
				notMarked[i][j] = true;

		// 全部水平墙状态初始化为"存在"
		for (int i = 0; i < this.height + 1; i++)
			for (int j = 0; j < this.width; j++)
				horizontal[i][j] = Wall.exist;

		// 全部竖直墙状态初始化为"存在"
		for (int i = 0; i < this.height; i++)
			for (int j = 0; j < this.width + 1; j++)
				vertical[i][j] = Wall.exist;

		// 迷宫左上角方格为入口,因此它的左墙为不存在
		vertical[0][0] = Wall.notExist;

		// 迷宫右下角为出口,因此它的右墙不存在
		vertical[this.height - 1][this.width] = Wall.notExist;

		// 随机生成迷宫.其实质是:拆除若干个水平墙和竖直墙以便能形成迷宫
		generate(0, 0);

		// 生成迷宫后立刻遍历,得到答案:checks[i][j]为Check.main的都是在主路径上
		// 分析Java程序设计教程书上的算法,是从首格开始的valid,但是本算法不是从首格开始.因此要从0,1开始,若不行则从1,0开始
		if (!traverse(0, 0, 0, 1))
			traverse(0, 0, 1, 0);

		// 遍历完后可能有些不在主路径上的方格未设置成Check.notMain,最好执行以下语句
		for (int i = 0; i < this.height; i++)
			for (int j = 0; j < this.width; j++)
				if (checks[i][j] != Check.main)
					checks[i][j] = Check.notMain;

	}

	/**
	 * 生成一个随机迷宫,本方法提供给构造方法及newMaze方法调用. 实质是:拆除若干个水平墙和竖直墙.算法来源:java2游戏设计,清华大学出版社
	 * 
	 * @param i
	 *            可以是0至height-1中任一数值,建议输入0
	 * @param j
	 *            可以是0至width-1中任一数值,建议输入0
	 */
	private void generate(int i, int j) {
		notMarked[i][j] = false;// 标记这个方格,以便不再访问

		Random gen = new Random();// 用于随机一个方向

		if (i == 0 && j == 0)// 左上角方格
			while (notMarked[i + 1][j] || notMarked[i][j + 1])// 只要下面的方格和右边方格有一个未标记就进入循环
			{
				switch (gen.nextInt(2)) {
				case 0:
					if (notMarked[i + 1][j])// 下面方格未标记
					{
						horizontal[i + 1][j] = Wall.notExist;// 拆除它们之间的墙,使它们连通
						generate(i + 1, j);
					}// 继续从此方格开始生成
					break;
				case 1:
					if (notMarked[i][j + 1])// 右边方格未标记
					{
						vertical[i][j + 1] = Wall.notExist;// 拆除它们之间的墙,使它们连通
						generate(i, j + 1);
					}// 继续从此方格开始生成
					break;
				}
			}

		if (i == this.height - 1 && j == 0)// 左下角方格
			while (notMarked[i - 1][j] || notMarked[i][j + 1])// 只要上面的方格和右边方格有一个未标记就进入循环
			{
				switch (gen.nextInt(2)) {
				case 0:
					if (notMarked[i - 1][j])// 上面方格未标记
					{
						horizontal[i][j] = Wall.notExist;// 拆除它们之间的墙,使它们连通
						generate(i - 1, j);
					}// 继续从此方格开始生成
					break;
				case 1:
					if (notMarked[i][j + 1])// 右边方格未标记
					{
						vertical[i][j + 1] = Wall.notExist;// 拆除它们之间的墙,使它们连通
						generate(i, j + 1);
					}// 继续从此方格开始生成
					break;
				}
			}

		// 以下算法同上,不详述.总之是分类:四个角,四条边,其余.

		if (i == 0 && j == this.width - 1)// 右上角方格
			while (notMarked[i + 1][j] || notMarked[i][j - 1]) {
				switch (gen.nextInt(2)) {
				case 0:
					if (notMarked[i + 1][j]) {
						horizontal[i + 1][j] = Wall.notExist;// 拆除它们之间的墙,使它们连通
						generate(i + 1, j);
					}// 继续从此方格开始生成
					break;
				case 1:
					if (notMarked[i][j - 1]) {
						vertical[i][j] = Wall.notExist;// 拆除它们之间的墙,使它们连通
						generate(i, j - 1);
					}// 继续从此方格开始生成
					break;
				}
			}

		if (i == this.height - 1 && j == this.width - 1)// 右下角方格
			while (notMarked[i - 1][j] || notMarked[i][j - 1]) {
				switch (gen.nextInt(2)) {
				case 0:
					if (notMarked[i - 1][j]) {
						horizontal[i][j] = Wall.notExist;// 拆除它们之间的墙,使它们连通
						generate(i - 1, j);
					}// 继续从此方格开始生成
					break;
				case 1:
					if (notMarked[i][j - 1]) {
						vertical[i][j] = Wall.notExist;// 拆除它们之间的墙,使它们连通
						generate(i, j - 1);
					}// 继续从此方格开始生成
					break;
				}
			}

		if (i == 0 && j != 0 && j != this.width - 1)// 第一行方格除了左上角和右上角
			while (notMarked[i][j + 1] || notMarked[i][j - 1]
					|| notMarked[i + 1][j]) {
				switch (gen.nextInt(3)) {
				case 0:
					if (notMarked[i][j + 1]) {
						vertical[i][j + 1] = Wall.notExist;
						generate(i, j + 1);
					}
					break;
				case 1:
					if (notMarked[i][j - 1]) {
						vertical[i][j] = Wall.notExist;
						generate(i, j - 1);
					}
					break;
				case 2:
					if (notMarked[i + 1][j]) {
						horizontal[i + 1][j] = Wall.notExist;
						generate(i + 1, j);
					}
					break;
				}
			}

		if (i == this.height - 1 && j != 0 && j != this.width - 1)// 最后一行方格除了左下角和右下角
			while (notMarked[i][j - 1] || notMarked[i][j + 1]
					|| notMarked[i - 1][j]) {
				switch (gen.nextInt(3)) {
				case 0:
					if (notMarked[i][j - 1]) {
						vertical[i][j] = Wall.notExist;
						generate(i, j - 1);
					}
					break;
				case 1:
					if (notMarked[i][j + 1]) {
						vertical[i][j + 1] = Wall.notExist;
						generate(i, j + 1);
					}
					break;
				case 2:
					if (notMarked[i - 1][j]) {
						horizontal[i][j] = Wall.notExist;
						generate(i - 1, j);
					}
					break;
				}
			}

		if (j == 0 && i != 0 && i != this.height - 1)// 第一列方格除了左上角和左下角
			while (notMarked[i - 1][j] || notMarked[i + 1][j]
					|| notMarked[i][j + 1]) {
				switch (gen.nextInt(3)) {
				case 0:
					if (notMarked[i - 1][j]) {
						horizontal[i][j] = Wall.notExist;
						generate(i - 1, j);
					}
					break;
				case 1:
					if (notMarked[i + 1][j]) {
						horizontal[i + 1][j] = Wall.notExist;
						generate(i + 1, j);
					}
					break;
				case 2:
					if (notMarked[i][j + 1]) {
						vertical[i][j + 1] = Wall.notExist;
						generate(i, j + 1);
					}
					break;
				}
			}

		if (j == this.width - 1 && i != 0 && i != this.height - 1)// 最后一列方格除了右上角和右下角
			while (notMarked[i - 1][j] || notMarked[i + 1][j]
					|| notMarked[i][j - 1]) {
				switch (gen.nextInt(3)) {
				case 0:
					if (notMarked[i - 1][j]) {
						horizontal[i][j] = Wall.notExist;
						generate(i - 1, j);
					}
					break;
				case 1:
					if (notMarked[i + 1][j]) {
						horizontal[i + 1][j] = Wall.notExist;
						generate(i + 1, j);
					}
					break;
				case 2:
					if (notMarked[i][j - 1]) {
						vertical[i][j] = Wall.notExist;
						generate(i, j - 1);
					}
					break;
				}
			}

		if (i > 0 && i < this.height - 1 && j > 0 && j < this.width - 1)// 除了最外面一层方格外的其余方格(即有(height-2)*(width-2)个)
			while (notMarked[i - 1][j] || notMarked[i + 1][j]
					|| notMarked[i][j - 1] || notMarked[i][j + 1]) {
				switch (gen.nextInt(4)) {
				case 0:
					if (notMarked[i - 1][j]) {
						horizontal[i][j] = Wall.notExist;
						generate(i - 1, j);
					}
					break;
				case 1:
					if (notMarked[i + 1][j]) {
						horizontal[i + 1][j] = Wall.notExist;
						generate(i + 1, j);
					}
					break;
				case 2:
					if (notMarked[i][j - 1]) {
						vertical[i][j] = Wall.notExist;
						generate(i, j - 1);
					}
					break;
				case 3:
					if (notMarked[i][j + 1]) {
						vertical[i][j + 1] = Wall.notExist;
						generate(i, j + 1);
					}
					break;
				}
			}
	}

	/**
	 * 新建一个迷宫,玩家移动,显示(取消)主路径,追赶者移动,拆墙,这些方法都会自动调用本方法来重画迷宫.
	 * 
	 * @param page
	 *            图形上下文
	 */
	public void paintComponent(Graphics page) {
		super.paintComponent(page);

		// 画水平的单位墙
		page.setColor(Color.black);
		for (int i = 0; i < this.height + 1; i++)
			for (int j = 0; j < this.width; j++)
				if (horizontal[i][j] == Wall.exist)// 这个水平单位墙存在才画
					page.drawLine(j * checkSize, i * checkSize, j * checkSize
							+ checkSize, i * checkSize);

		// 画竖直的单位墙
		page.setColor(Color.black);
		for (int i = 0; i < this.height; i++)
			for (int j = 0; j < this.width + 1; j++)
				if (vertical[i][j] == Wall.exist)// 这个竖直单位墙存在才画
					page.drawLine(j * checkSize, i * checkSize, j * checkSize,
							i * checkSize + checkSize);
		// 画终点
		page.setColor(Color.darkGray);
		page.drawString("终点", this.width * checkSize, this.height * checkSize);

		if (!solve) {// 如果解答为关,画玩家位置
			page.setColor(Color.blue);
			page.fillOval(playerColumn * checkSize, playerRow * checkSize,
					checkSize, checkSize);
		}

		if (chase && !solve) {// 如果解答为关且追赶者为开则画追赶者
			page.setColor(Color.red);
			for (Chaser chaser : chasers)
				page.fillOval(chaser.getColumn() * checkSize,
						chaser.getRow() * checkSize, checkSize, checkSize);
		}

		if (solve) {// 如果解答为"开",就画出主路径
			page.setColor(Color.green);
			for (int i = 0; i < this.height; i++)
				for (int j = 0; j < this.width; j++)
					if (checks[i][j] == Check.main)// 方格在主路径上
						page.fillOval(j * checkSize, i * checkSize, checkSize,
								checkSize);
		}

	}

	/**
	 * 在本迷宫面板上重新随机生成一个迷宫.设置同初始化的一样
	 * 这是新生成迷宫的提示:这是一个迷宫游戏.玩家是蓝点,走到右下角就赢.若途中碰到两个红点追赶者就输.请按键盘的方向键移动
	 * (建议新生成迷宫时给予玩家提示)
	 * 
	 * @param checkSize
	 *            (迷宫由单位方格构成)单位方格的边长,以像素输入
	 * @param width
	 *            迷宫的宽度.例如:宽度若为3即宽度为3个单位方格
	 * @param height
	 *            迷宫的高度.例如:高度若为3即高度为3个单位方格
	 * @param numberOfChasers
	 *            追赶者的数量(不要追赶者则为0)
	 * @param tearDownAllowed
	 *            玩家可拆墙次数限制(不可拆墙则为0,可拆无数次则为-1)
	 *  @param timeSurplus
	 *  		游戏剩余时间，倒计时
	 */
	public void newMaze(int checkSize, int width, int height,
			int numberOfChasers, int tearDownAllowed,int timeSurplus) {
		this.checkSize = checkSize;
		this.width = width;
		this.height = height;
		this.tearDownAllowed = tearDownAllowed;
		this.timeSurplus = timeSurplus;
		this.playerRow = 0;
		this.playerColumn = 0;// 玩家初始在左上角的方格:起点
		this.solve = false;// 不打印结果
		this.timer = new Timer(DELAY, new TimerListener());// 追赶者移动定时器

		if (numberOfChasers < 1) {
			this.chase = false;// 玩家设定追赶者为"关"
			chasers = new Chaser[0];
		} else {
			this.chase = true;
			chasers = new Chaser[numberOfChasers];// 初始化追赶者
			for (int i = 0; i < numberOfChasers; i++) {
				Random gen = new Random();
				int ranRow, ranColumn;
				ranRow = gen.nextInt(this.height);
				ranColumn = gen.nextInt(this.width);
				chasers[i] = new Chaser(ranRow, ranColumn);
			}
			chaseOn();// 开启追赶者,使其移动
		}

		this.currentResult = 0;// 游戏进行中
		this.checks = new Check[this.height][this.width];// 迷宫为height*width个方格
		this.horizontal = new Wall[this.height + 1][this.width];
		// 单位水平墙.horizontalWalls有height*width-1个
		this.vertical = new Wall[this.height][this.width + 1];
		// 单位竖直墙.horizontalWalls有(height-1)*width个
		this.notMarked = new boolean[this.height][this.width];
		// 单位方格是否有标记过(在随机生成迷宫时需要)

		// 以下为迷宫具体初始化:
		// 全部方格状态初始化为"未踏足过"
		for (int i = 0; i < this.height; i++)
			for (int j = 0; j < this.width; j++)
				checks[i][j] = Check.notFoot;

		// 全部方格初始化为"未标记"
		for (int i = 0; i < this.height; i++)
			for (int j = 0; j < this.width; j++)
				notMarked[i][j] = true;

		// 全部水平墙状态初始化为"存在"
		for (int i = 0; i < this.height + 1; i++)
			for (int j = 0; j < this.width; j++)
				horizontal[i][j] = Wall.exist;

		// 全部竖直墙状态初始化为"存在"
		for (int i = 0; i < this.height; i++)
			for (int j = 0; j < this.width + 1; j++)
				vertical[i][j] = Wall.exist;

		// 迷宫左上角方格为入口,因此它的左墙为不存在
		vertical[0][0] = Wall.notExist;

		// 迷宫右下角为出口,因此它的右墙不存在
		vertical[this.height - 1][this.width] = Wall.notExist;

		// 随机生成迷宫.其实质是:拆除若干个水平墙和竖直墙以便能形成迷宫
		generate(0, 0);

		// 生成迷宫后立刻遍历,得到答案:checks[i][j]为Check.main的都是在主路径上
		// 分析Java程序设计教程书上的算法,是从首格开始的valid,但是本算法不是从首格开始.因此要从0,1开始,若不行则从1,0开始
		if (!traverse(0, 0, 0, 1))
			traverse(0, 0, 1, 0);

		// 遍历完后可能有些不在主路径上的方格未设置成Check.notMain,最好执行以下语句
		for (int i = 0; i < this.height; i++)
			for (int j = 0; j < this.width; j++)
				if (checks[i][j] != Check.main)
					checks[i][j] = Check.notMain;

		// 新迷宫:重画
		repaint();
	}

	/**
	 * 无墙模式:新建一个没有墙的迷宫,玩家闪避过所有追赶者,走到右下角终点就赢.
	 * 
	 * @param checkSize
	 *            (迷宫由单位方格构成)单位方格的边长,以像素输入
	 * @param width
	 *            迷宫的宽度.例如:宽度若为3即宽度为3个单位方格
	 * @param height
	 *            迷宫的高度.例如:高度若为3即高度为3个单位方格
	 * @param numberOfChasers
	 *            追赶者的数量(不建议为0)
	 * @param delay
	 *            追赶者移动延迟时间(即每隔多少时间移动一次).单位:毫秒
	 */
	public void noWalls(int checkSize, int width, int height,
			int numberOfChasers,int delay){
		this.checkSize = checkSize;
		this.width = width;
		this.height = height;
		this.playerRow = 0;
		this.playerColumn = 0;// 玩家初始在左上角的方格:起点
		this.solve = false;// 不打印结果
		this.timer = new Timer(delay, new TimerListener());// 追赶者移动定时器

		if (numberOfChasers < 1) {
			this.chase = false;// 玩家设定追赶者为"关"
			chasers = new Chaser[0];
		} else {
			this.chase = true;
			chasers = new Chaser[numberOfChasers];// 初始化追赶者
			for (int i = 0; i < numberOfChasers; i++) {
				Random gen = new Random();
				int ranRow, ranColumn;
				ranRow = gen.nextInt(this.height);
				ranColumn = gen.nextInt(this.width);
				chasers[i] = new Chaser(ranRow, ranColumn);
			}
			chaseOn();// 开启追赶者,使其移动
		}

		this.currentResult = 0;// 游戏进行中
		this.checks = new Check[this.height][this.width];// 迷宫为height*width个方格
		this.horizontal = new Wall[this.height + 1][this.width];
		// 单位水平墙.horizontalWalls有height*width-1个
		this.vertical = new Wall[this.height][this.width + 1];
		// 单位竖直墙.horizontalWalls有(height-1)*width个
		this.notMarked = new boolean[this.height][this.width];
		// 单位方格是否有标记过(在随机生成迷宫时需要)

		// 以下为迷宫具体初始化:
		// 全部方格状态初始化为"未踏足过"
		for (int i = 0; i < this.height; i++)
			for (int j = 0; j < this.width; j++)
				checks[i][j] = Check.notFoot;

		// 全部方格初始化为"未标记"
		for (int i = 0; i < this.height; i++)
			for (int j = 0; j < this.width; j++)
				notMarked[i][j] = true;

		// 全部内部水平墙状态初始化为"不存在"
		for (int i = 0; i < this.height + 1; i++)
			for (int j = 0; j < this.width; j++)
				if(i>0&&i<this.height+1)
					horizontal[i][j] = Wall.notExist;
				else
				horizontal[i][j] = Wall.exist;

		// 全部内部竖直墙状态初始化为"不存在"
		for (int i = 0; i < this.height; i++)
			for (int j = 0; j < this.width + 1; j++)
				if(j>0&&j<this.width+1)
					vertical[i][j] = Wall.notExist;
				else
				vertical[i][j] = Wall.exist;

		// 迷宫左上角方格为入口,因此它的左墙为不存在
		vertical[0][0] = Wall.notExist;

		// 迷宫右下角为出口,因此它的右墙不存在
		vertical[this.height - 1][this.width] = Wall.notExist;

		// 新无墙迷宫:重画
		repaint();
	}
	
	/**
	 * 本方法供traverse方法调用,用于判断从i1行j1列的方格走到i2行j2列的方格是否可行
	 * 
	 * @param i1
	 *            原方格所在行数
	 * @param j1
	 *            原方格所在列数
	 * @param i2
	 *            目标方格所在行数
	 * @param j2
	 *            目标方格所在列数
	 * @return 从原格走到目标格可行则返回true;否则返回false
	 */
	private boolean valid(int i1, int j1, int i2, int j2) {
		boolean result = false;

		if (i2 >= 0 && i2 < this.height && j2 >= 0 && j2 < this.width)
			if (checks[i2][j2] == Check.notFoot) {// 保证没有越界而且目标方格未踏足过
				if (i1 == i2 - 1 && horizontal[i2][j2] == Wall.notExist)// 从上往下
					result = true;
				else if (i1 == i2 + 1 && horizontal[i1][j1] == Wall.notExist)// 从下往上
					result = true;
				else if (j1 == j2 - 1 && vertical[i2][j2] == Wall.notExist)// 从左往右
					result = true;
				else if (j1 == j2 + 1 && vertical[i1][j1] == Wall.notExist)// 从右往左
					result = true;
			}

		return result;

	}

	/**
	 * 遍历迷宫,遍历完成后就能找到主路径:checks[i][j]为Check.main的方格都在主路径上
	 * 
	 * @param i1
	 *            原方格所在行数
	 * @param j1
	 *            原方格所在列数
	 * @param i2
	 *            目标方格所在行数
	 * @param j2
	 *            目标方格所在列数
	 * @return 找到从起点到终点路径则返回true;否则返回false
	 */
	private boolean traverse(int i1, int j1, int i2, int j2) {
		boolean done = false;

		if (i1 == 0 && j1 == 0)// 起点
			checks[i1][j1] = Check.main;

		if (valid(i1, j1, i2, j2)) {
			checks[i2][j2] = Check.notMain;// 先设定为notMain表示不在主路径上

			if (i2 == this.height - 1 && j2 == this.width - 1)
				done = true; // 到达终点
			else {
				done = traverse(i2, j2, i2 + 1, j2); // down
				if (!done)
					done = traverse(i2, j2, i2, j2 + 1); // right
				if (!done)
					done = traverse(i2, j2, i2 - 1, j2); // up
				if (!done)
					done = traverse(i2, j2, i2, j2 - 1); // left
			}

			if (done) // 若done为true说明这个方格在主路径上
				checks[i2][j2] = Check.main;
		}

		return done;
	}

	/**
	 * 开启解答:迷宫中将会显示解答
	 */
	public void solveOn() {
		solve = true;
		repaint();
	}

	/**
	 * 关闭解答:迷宫中将不显示解答
	 */
	public void solveOff() {
		solve = false;
		repaint();
	}

	/**
	 * 玩家移动(当然,有墙阻挡就走不了那个方向)
	 * 
	 * @param direction
	 *            1--向左走,2--向上走,3--向右走,4--向下走
	 * @return 成功走了一步则返回true;否则返回false
	 */
	public boolean move(int direction) {
		boolean succeed = false;

		switch (direction) {
		case 1:
			if (playerColumn != 0
					&& vertical[playerRow][playerColumn] == Wall.notExist) {
				playerColumn -= 1;
				succeed = true;
			}
			break;
		case 2:
			if (playerRow != 0
					&& horizontal[playerRow][playerColumn] == Wall.notExist)// 不在第一行且没有墙阻挡
			{
				playerRow -= 1;
				succeed = true;
			}
			break;
		case 3:
			if (playerColumn != this.width - 1
					&& vertical[playerRow][playerColumn + 1] == Wall.notExist) {
				playerColumn += 1;
				succeed = true;
			}
			break;
		case 4:
			if (playerRow != this.height - 1
					&& horizontal[playerRow + 1][playerColumn] == Wall.notExist) {
				playerRow += 1;
				succeed = true;
			}
			break;
		}

		repaint();// 走完一步不要忘记重画!

		return succeed;
	}

	/**
	 * 开启追赶者
	 */
	public void chaseOn() {
		this.chase = true;
		timer.start();
		repaint();
	}

	/**
	 * 关闭追赶者
	 */
	public void chaseOff() {
		this.chase = false;
		timer.stop();
		repaint();
	}

	/**
	 * 玩家拆墙,若超过了限制次数或本来就没墙的则拆墙无效
	 * 
	 * @param direction
	 *            1--拆玩家当前方格位置的左墙,2--拆上墙,3--拆右墙,4--拆下墙
	 */
	public void tearDown(int direction) {
		boolean succeed = false;

		if (this.tearDownAllowed != 0) {// 若小于0则说明玩家设定拆墙不限次数,若大于0则说明还可以拆墙
			switch (direction) {
			case 1:
				if (vertical[playerRow][playerColumn] == Wall.exist) {
					vertical[playerRow][playerColumn] = Wall.notExist;
					succeed = true;
				}
				break;
			case 2:
				if (horizontal[playerRow][playerColumn] == Wall.exist) {
					horizontal[playerRow][playerColumn] = Wall.notExist;
					succeed = true;
				}
				break;
			case 3:
				if (vertical[playerRow][playerColumn + 1] == Wall.exist) {
					vertical[playerRow][playerColumn + 1] = Wall.notExist;
					succeed = true;
				} 
				break;
			case 4:
				if (horizontal[playerRow + 1][playerColumn] == Wall.exist) {
					horizontal[playerRow + 1][playerColumn] = Wall.notExist;
					succeed = true;
				} 
				break;
			}
			if (this.tearDownAllowed != -1 && succeed)
				this.tearDownAllowed--;// 是限制次数且成功拆了一个墙
		}

		repaint();// 拆完墙或建完墙后不要忘记重画!
	}

	/**
	 * 获取当前可拆墙剩余次数
	 * 
	 * @return 当前可拆墙剩余次数
	 */
	public int tearDownAllowedTimes() {
		return tearDownAllowed;
	}
	
	public int timeSurplusTimes() {
		return timeSurplus;
	}
	

	/**
	 * 可变范围拆墙,拆除一个矩形区域内的所有墙:以玩家当前位置为中心方格,以width为宽度半径,以height为高度半径
	 * 注意:若给定矩形区域不完全在迷宫区域内则拆墙无效
	 * 
	 * @param widthHalf
	 *            矩形宽度半径
	 * @param heightHalf
	 *            矩形高度半径
	 */
	public void variableTearDown(int widthHalf, int heightHalf) {
		boolean notBeyond;

		notBeyond = playerRow - heightHalf >= 0
				&& playerRow + heightHalf + 1 <= this.height
				&& playerColumn - widthHalf >= 0
				&& playerColumn + widthHalf + 1 <= this.width;

		if (notBeyond) {// 未超出迷宫范围
			// 矩形区域内的水平墙全拆除
			for (int i = playerRow - heightHalf; i <= playerRow + heightHalf
					+ 1; i++)
				for (int j = playerColumn - widthHalf; j <= playerColumn
						+ widthHalf; j++)
					horizontal[i][j] = Wall.notExist;

			// 矩形区域内的竖直墙全拆除
			for (int i = playerRow - heightHalf; i <= playerRow + heightHalf; i++)
				for (int j = playerColumn - widthHalf; j <= playerColumn
						+ widthHalf + 1; j++)
					vertical[i][j] = Wall.notExist;
		}

		repaint();// 再次强调:拆完墙不要忘记重画!

	}

	/**
	 * 保护生效:玩家四面是墙;保护失效:玩家四面无墙
	 * 
	 * @param protect
	 *            true为保护生效;false为保护失效
	 */
	public void protectEnabled(boolean protect) {
		if (protect) {
			horizontal[playerRow][playerColumn] = Wall.exist;
			horizontal[playerRow + 1][playerColumn] = Wall.exist;
			vertical[playerRow][playerColumn] = Wall.exist;
			vertical[playerRow][playerColumn + 1] = Wall.exist;
		} else {
			horizontal[playerRow][playerColumn] = Wall.notExist;
			horizontal[playerRow + 1][playerColumn] = Wall.notExist;
			vertical[playerRow][playerColumn] = Wall.notExist;
			vertical[playerRow][playerColumn + 1] = Wall.notExist;
		}

		repaint();
	}

	/**
	 * 返回当前的游戏结果(考虑到玩家不动却被追赶成功的情况,不推荐在每次玩家移动时作出判断)
	 * (建议在外部类中定义一个定时器,每隔10毫秒就判断一次玩家的输赢状态,方便处理)
	 * 
	 * @return 返回-1表示当前玩家输了(追赶者为"开"且玩家碰到追赶者)
	 *         返回0表示当前迷宫游戏仍在进行中;返回1表示当前玩家赢了(玩家到达终点且此刻并没有碰到追赶者)
	 */
	public int currentResult() {
		boolean chased = false;// 判断是否被追到

		for (Chaser chaser : chasers)
			if (chase == true && playerRow == chaser.getRow()
					&& playerColumn == chaser.getColumn())
				chased = true;

		if (chased)// 追赶者为"开"且玩家碰到追赶者
			currentResult = -1;
		else if (playerRow == this.height - 1 && playerColumn == this.width - 1)// 玩家到达终点且此刻并没有碰到追赶者
			currentResult = 1;

		return currentResult;
	}

	/**
	 * 当前提示,会根据玩家在迷宫的当前位置给出提示 (考虑到玩家不动却被追赶成功的情况,不推荐在每次玩家移动时给出提示)
	 * (建议在外部类中定义一个定时器,每隔10毫秒就调用一次本方法返回当前提示)
	 * 
	 * @return 当前提示,会根据玩家在迷宫的当前位置给出提示.
	 */
	public String currentTip() {
		String tip = "暂 无 提 示";
		boolean chaserNear = false;// 判断追赶者是否在附近

		for (Chaser chaser : chasers)
			if (Math.abs(playerRow - chaser.getRow()) < 2
					&& Math.abs(playerColumn - chaser.getColumn()) < 2)
				chaserNear = true;

		if (playerRow == 0 && playerColumn == 0)// 玩家在起点
			tip = "你 在 起 点";
		else if (this.currentResult() == 1)// 玩家到达终点
			tip = "恭 喜 到 达 终 点!";
		else if (chaserNear)// 追赶者和玩家相差几个方格
			tip = "当 心! 追 赶 者 似 乎 很 接 近 了!";
		else if (checks[playerRow][playerColumn] == Check.notMain) // 玩家当前位置不在主路径上
			tip = "你 走 的 路 似 乎 不 对";
		else if (checks[playerRow][playerColumn] == Check.main)// 玩家当前位置在主路径上
			tip = "你 走 的 路 似 乎 是 正 确 的";

		return tip;
	}

	/**
	 * 帮助信息:迷宫的规则.注意:按键功能由外部类定义并告诉玩家.
	 * 
	 * @return 帮助信息:迷宫的规则
	 */
	public String help() {
		String help;

		help = "规 则 : 玩 家 是 蓝 点, 碰 到 红 点 就 输,走 到 右 下 角 (终 点) 就 赢.";

		return help;
	}

	/**
	 * 追赶者的定时移动
	 * 
	 * @author 胡
	 * 
	 */
	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			for (Chaser chaser : chasers)
				// 追赶者追赶玩家
				chaser.chase();

			repaint();// 追赶者移动后不要忘记重画!
		}
	}

	/**
	 * 定义一个追赶者以及其追赶操作
	 * 
	 * @author 胡
	 * 
	 */
	private class Chaser {
		private int row, column;// 追赶者的所在方格的行数和列数,可在Maze类中访问

		/**
		 * 新的追赶者
		 * 
		 * @param row
		 *            新建追赶者所在方格的行数
		 * @param column
		 *            新建追赶者所在方格的列数
		 */
		private Chaser(int row, int column) {
			this.row = row;
			this.column = column;
		}

		/**
		 * 以追赶者为中心,玩家的方位有8个:上,左上,右等,追赶者就是根据这些来追赶
		 */
		private void chase() {
			if (playerRow == this.row && playerColumn < this.column)// 玩家在追赶者左边
				this.move(8);
			else if (playerRow == this.row && playerColumn > this.column)// 玩家在追赶者右边
				this.move(4);
			else if (playerRow < this.row && playerColumn == this.column)// 玩家在追赶者上面
				this.move(2);
			else if (playerRow > this.row && playerColumn == this.column)// 玩家在追赶者下面
				this.move(6);
			else if (playerRow < this.row && playerColumn < this.column)// 玩家在追赶者左上方
				this.move(1);
			else if (playerRow < this.row && playerColumn > this.column)// 玩家在追赶者右上方
				this.move(3);
			else if (playerRow > this.row && playerColumn < this.column)// 玩家在追赶者左下方
				this.move(7);
			else if (playerRow > this.row && playerColumn > this.column)// 玩家在追赶者右下方
				this.move(5);
		}

		/**
		 * 追赶者移动
		 * 
		 * @param direction
		 *            1--左上方,2--上方,3--右上方,4--右方,5--右下方,6--下方,7--左下方,8--左方
		 * @return 成功走了一步则返回true;否则返回false
		 */
		private boolean move(int direction) {
			boolean succeed = false;

			switch (direction) {
			case 1:
				succeed = this.move(8);
				if (!succeed)
					succeed = this.move(2);
				break;

			case 2:
				if (row != 0 && horizontal[row][column] == Wall.notExist)// 不在第一行且没有墙阻挡
				{
					row -= 1;
					succeed = true;
				}
				break;

			case 3:
				succeed = this.move(4);
				if (!succeed)
					succeed = this.move(2);
				break;

			case 4:
				if (column != width - 1
						&& vertical[row][column + 1] == Wall.notExist) {
					column += 1;
					succeed = true;
				}
				break;

			case 5:
				succeed = this.move(4);
				if (!succeed)
					succeed = this.move(6);
				break;

			case 6:
				if (row != height - 1
						&& horizontal[row + 1][column] == Wall.notExist) {
					row += 1;
					succeed = true;
				}
				break;

			case 7:
				succeed = this.move(8);
				if (!succeed)
					succeed = this.move(6);
				break;

			case 8:
				if (column != 0 && vertical[row][column] == Wall.notExist) {
					column -= 1;
					succeed = true;
				}
				break;
			}

			return succeed;
		}
		
		/**
		 * 
		 * @return 追赶者当前方格的行数
		 */

		public int getRow() {
			return row;
		}
		
		/**
		 * 
		 * @return 追赶者当前方格的列数
		 */

		public int getColumn() {
			return column;
		}
	}
}